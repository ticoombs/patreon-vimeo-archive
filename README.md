# Patreon - Vimeo Archiver

Uses python and selenium to load patreon. Login using your credentials. Then checks for vimeo videos with the episode name.

I originally created this because I have terrible internet on my portable devices, and wanted a way to watch the videos that did not require the patreon application as I had zero devices that were supported.

So just archive the vidoes and then sync to your portable device

Warranty: There is no warranty!

License: If you use this code you are required to donate your monthly patreon costs to a local food/soup kitchen. Once per month.
IE. If you are currently paying $10/month to 5 patreon for a total of $50. You are required to donate $50 per month of usage.

## Usage

### Setup

Edit patreon.py with your:
- username
- password
- specific episode naming
- firefox/chrome location

Install youtube-dl 

### Environment

```
# initialise a virtual env
virtualenv env
pip3 install -r requirements.txt
```

### Run it!

```
python3 patreon.py
# It will output the ID of the video(s). Now download that ID via youtube-dl with the correct settings
youtube-dl $id 
```

Checkout the included wrapper script for how you would meld the two together
`./patreon-vimeo.sh`
