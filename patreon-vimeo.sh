#!/bin/bash

function ytdlPatreon {
    youtube-dl -f 'bestvideo[height=720]+bestaudio[ext=m4a]'\
        https://player.vimeo.com/video/$1 \
        --referer https://www.patreon.com/ \
        --download-archive $HOME/.cache/youtube-dl.log
}

if ! [ -z $1 ]; then
    id=$1
else
    # activate our patreon amazingness
    source ./env/bin/activate
    id=$(python3 patreon.py)
fi

ytdlPatreon "$id"
