from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from sys import exit
from sys import stderr
import re


def eprint(*args, **kwargs):
    print(*args, file=stderr, **kwargs)


URL = 'https://www.patreon.com/'
username = ''
password = ''
EPISODE = ''

options = Options()
options.headless = True

driver = webdriver.Firefox(options=options, firefox_binary="/opt/firefox/firefox")
driver.set_window_size(1024, 768)

driver.get(URL + 'login')
driver.find_element_by_css_selector('#email').send_keys(username)
driver.find_element_by_css_selector('#password').send_keys(password)
driver.find_element_by_xpath('//button[@type="submit"]').click()

# driver.implicitly_wait(15) # seconds
# wait for page to load
try:
    WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, EPISODE))
    )
except Exception as e:
    eprint("Failed to find a {} episode. E: {}".format(EPISODE, e))
    eprint("Current URL: {}".format(driver.current_url))
    driver.close()
    driver.quit()
    exit(1)


# grab the links
videos = driver.find_elements_by_partial_link_text(EPISODE)
# print the urls
for url in ([a.get_attribute("href") for a in videos]):
    driver.get(url)
    WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//button"))
    )
    driver.find_element_by_xpath('//button[@title="Start playback"]').click()
    WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//iframe"))
    )
    element = driver.find_element_by_xpath('//iframe')
    main_url = element.get_attribute("src")
    video_id = re.search('https%3A%2F%2Fplayer.vimeo.com%2Fvideo%2F([0-9]+)%3F', main_url)
    # print(main_url)
    print(video_id.group(1))
    break

driver.close()
driver.quit()
